import Start from "@/components/Start/Start.vue";
import Question from "@/components/Question/Question.vue";
import Result from "@/components/Result/Result.vue";
import VueRouter from "vue-router";


const routes = [
    {
        path: "/",
        alias: "/start",
        name: "Start",
        component: Start,
    },
    {
        path: "/question",
        name: "Question",
        component: Question,
    },
    {
        path: "/result",
        name: "Result",
        component: Result,
    },
];

const router = new VueRouter({ routes });

export default router;
