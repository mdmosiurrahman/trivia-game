import Vue from 'vue';
import Vuex from 'vuex';

import { TriviaAPI } from '@/components/GameApi/TriviaAPI';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		active: false,
		options: {},
		categories: [],
		questions: null,
		userAnswers: [],
		step: 0,
		score: 0,
		error: '',
	},
	mutations: {
		setActive: (state, payload) => {
			state.active = payload;
		},
		setOptions: (state, payload) => {
			state.options = payload;
		},
		setCategories: (state, payload) => {
			state.categories = payload;
		},
		setQuestions: (state, payload) => {
			state.questions = payload;
		},
		resetUserAnswers: state => {
			state.userAnswers = [];
		},
		setUserAnswer: (state, payload) => {
			state.userAnswers.push(payload);
		},
		setStep: (state, payload) => {
			state.step = payload;
		},
		setScore: (state, payload) => {
			state.score = payload;
		},
		setError: (state, payload) => {
			state.error = payload;
		},
	},
	getters: {
		getCurrentQuestion: state => {
			return state.questions !== null ? state.questions[state.step] : null;
		},
		getCurrentAnswers: state => {
			return ([
				...state.questions[state.step].incorrect_answers,
				state.questions[state.step].correct_answer,
			]);
		},
	},
	actions: {
		async fetchQuestions({ commit, state }, router) {
			const { options } = state;
			try {
				const questions = await TriviaAPI.fetchQuestions(options);
				if (questions.length) {
					commit('setQuestions', questions);
					commit('setActive', true);
					commit('setScore', 0);
					commit('setStep', 0);
					commit('setError', '');
					commit('resetUserAnswers');
					router.push('/question');
				} else {
					commit('setError', ' no questions retrive with those options');
				}
			} catch (e) {
				commit('setError', e.message);
			}
		},
		async fetchCategories({ commit }) {
			try {
				const categories = await TriviaAPI.fetchCategories();
				commit('setCategories', categories);
			} catch (e) {
				commit('setError', e.message);
			}
		},
	},
});
